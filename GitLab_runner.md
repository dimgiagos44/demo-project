# commands for gitlab runner setup on a Kubernetes cluster
1. ``` helm repo add gitlab https://charts.gitlab.io ```

2. ``` helm init ```

3. create a values.yaml for the gitlab-runner deployment

4. get gitlab-url and runner-token for settings/CI-CD/runners and set privileged: true

5. ``` helm install -n gitlab-runner-ns gitlab-runner -f values.yaml gitlab/gitlab-runner --set rbac.create=true ```

6. ``` helm upgrade --install --namespace gitlab-com gitlab-runner --set gitlabUrl=https://gitlab.com/ --set runnerRegistrationToken=<TOKEN> --set rbac.create=true gitlab/gitlab-runner ```

7. ``` kubectl create rolebinding default-view --clusterrole=edit --serviceaccount=gitlab-managed-apps:default --namespace=gitlab-managed-apps ```
[here](https://stackoverflow.com/questions/53393853/gitlab-ci-pipeline-cannot-create-pods-in-the-namespace)

8. In a directory of the project's repo, we can kubectl apply -f directory/file.yaml -n gitlab-runner-ns in order to create deployment in our cluster via gitlab pipeline. The appropriate images for kubectl, docker etc are needed.
